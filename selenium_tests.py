import time
import random
import string
import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select


USERNAME = '***'
PASSWORD = '***'



def login(driver, username, password):
    driver.get('https://is.mk.cvut.cz')
    driver.find_element_by_name('username').send_keys(username)
    driver.find_element_by_name('password').send_keys(password)
    driver.find_element_by_xpath("//button[@type='submit']").click()
    element = WebDriverWait(driver, 10).until(EC.title_contains('Profile | Masarka Student Club'))
    
def login_admin(driver):
    login(driver, USERNAME, PASSWORD)
    
def logout(driver):
    driver.find_element_by_class_name('mdi-logout').click()
    WebDriverWait(driver, 10).until(EC.title_contains('Sign in | Masarka Student Club'))
    
def update_profile(driver):
    login_admin(driver)
    clear_element_text(driver.find_elements_by_tag_name('input')[4])
    driver.find_elements_by_tag_name('input')[4].send_keys('admin@mk.cvut.cz')
    clear_element_text(driver.find_elements_by_tag_name('input')[5])
    driver.find_elements_by_tag_name('input')[5].send_keys('_R516')
    driver.find_element_by_xpath("//*[contains(text(), 'Save')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Profile | Masarka Student Club'))
    
def add_new_device(driver):
    login(driver, USERNAME, PASSWORD)       
    driver.find_element_by_xpath("//*[contains(text(), 'Devices')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Devices | Masarka Student Club'))
    driver.find_element_by_xpath("//*[contains(text(), 'New item')]").click()
    driver.find_elements_by_tag_name('input')[2].send_keys('test')
    driver.find_elements_by_tag_name('input')[3].send_keys('AA:AA:AA:AA:AA:AA')
    driver.find_elements_by_class_name('mdi-menu-down')[1].click()
    driver.find_elements_by_class_name('v-list-item')[-2].click()
    driver.find_element_by_xpath("//*[contains(text(), 'Save')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Devices | Masarka Student Club'))
    
def add_new_reservation(driver):
    login(driver, USERNAME, PASSWORD)    
    driver.find_element_by_xpath("//*[contains(text(), 'Reservations')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Reservations | Masarka Student Club'))
    driver.find_element_by_xpath("//*[contains(text(), 'New reservation')]").click()
    driver.find_elements_by_class_name('mdi-menu-down')[2].click()
    driver.find_element_by_xpath("//*[contains(text(), 'Gym')]").click()
    driver.find_elements_by_class_name('mdi-menu-down')[3].click()
    driver.find_elements_by_class_name('v-list-item')[17].click()
    driver.find_elements_by_class_name('mdi-menu-down')[4].click()
    driver.find_elements_by_class_name('v-list-item')[32].click()
    driver.find_element_by_xpath("//*[contains(text(), 'Save')]").click()
        
def open_admin_panel(driver, page='Registrations'):
    login_admin(driver)
    driver.find_element_by_xpath("//*[contains(text(), 'Admin panel')]").click()
    element = WebDriverWait(driver, 10).until(EC.title_contains('Registrations | Masarka Student Club'))        
    if page is not 'Registrations':    
        driver.find_element_by_xpath("//*[contains(text(), '" + page + "')]").click()
        WebDriverWait(driver, 10).until(EC.title_contains(page + ' | Masarka Student Club'))
    
def create_new_preregistration_item(driver, email):
    driver.get('https://is.mk.cvut.cz')
    driver.find_element_by_xpath("//*[contains(text(), 'Pre-registration')]").click()
    element = WebDriverWait(driver, 10).until(EC.title_contains('Pre-registration | Masarka Student Club'))
    driver.find_elements_by_tag_name('input')[0].send_keys('Your Name')
    driver.find_elements_by_tag_name('input')[1].send_keys('Your Surname')
    driver.find_elements_by_tag_name('input')[2].send_keys(email)
    driver.find_element_by_xpath("//input[@type='date']").send_keys('2000-10-01')
    driver.find_elements_by_tag_name('input')[4].send_keys('A001')
    driver.find_elements_by_tag_name('input')[5].click()
    driver.find_element_by_xpath("//*[contains(text(), 'Angola')]").click()
    driver.find_elements_by_tag_name('input')[7].send_keys('NewCity')
    driver.find_elements_by_tag_name('input')[8].click()
    driver.find_elements_by_xpath("//*[contains(text(), 'Angola')]")[1].click()
    driver.find_elements_by_tag_name('input')[10].send_keys('NewCity')
    driver.find_elements_by_tag_name('input')[11].send_keys('26000')
    driver.find_element_by_tag_name('textarea').send_keys('Flower str. 26')
    driver.find_element_by_xpath("//button[@type='submit']").click()
    element = WebDriverWait(driver, 10).until(EC.title_contains('Sign in | Masarka Student Club'))
    wait_for_data_to_load(2)
    

def edit_preregistration_item(driver, email):
    driver.get('https://is.mk.cvut.cz')
    open_admin_panel(driver)
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '" + email + "')]")))
    driver.find_element_by_xpath("//*[contains(text(), '" + email + "')]").find_element_by_xpath("./../td[9]/button[2]").click()
    WebDriverWait(driver, 10).until(EC.title_contains("Edit Registration"))
    wait_for_data_to_load()
    room_number_input_element = driver.find_elements_by_tag_name('input')[3]
    clear_element_text(room_number_input_element)
    room_number_input_element.send_keys('R001')
    driver.find_element_by_xpath("//*[contains(text(), 'Save')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Registrations | Masarka Student Club'))
    
def delete_preregistration_item(driver, email):
    driver.get('https://is.mk.cvut.cz')
    open_admin_panel(driver)
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '" + email + "')]")))
    driver.find_element_by_xpath("//*[contains(text(), '" + email + "')]").find_element_by_xpath("./../td[9]/button[3]").click()    
    driver.find_element_by_xpath("//*[contains(text(), 'Yes')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Registrations | Masarka Student Club'))
    

def complete_preregistration(driver, email):
    driver.get('https://is.mk.cvut.cz')
    open_admin_panel(driver)
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '" + email + "')]")))
    driver.find_element_by_xpath("//*[contains(text(), '" + email + "')]").find_element_by_xpath("./../td[9]/button[1]").click()    
    driver.find_element_by_xpath("//*[contains(text(), 'Save')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Users | Masarka Student Club'))
    
    
def edit_user_item(driver, email):
    driver.get('https://is.mk.cvut.cz')
    open_admin_panel(driver, 'Users')
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '" + email + "')]")))
    driver.find_element_by_xpath("//*[contains(text(), '" + email + "')]").find_element_by_xpath("./../td[12]/button[3]").click()
    WebDriverWait(driver, 10).until(EC.title_contains("Edit User"))
    wait_for_data_to_load()
    room_number_input_element = driver.find_elements_by_tag_name('input')[4]
    clear_element_text(room_number_input_element)
    room_number_input_element.send_keys('R001')
    driver.find_element_by_xpath("//*[contains(text(), 'Save')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Users | Masarka Student Club'))
    
    
def reset_user_password(driver, email):
    driver.get('https://is.mk.cvut.cz')
    open_admin_panel(driver, 'Users')
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '" + email + "')]")))
    driver.find_element_by_xpath("//*[contains(text(), '" + email + "')]").find_element_by_xpath("./../td[12]/button[1]").click()
    driver.find_element_by_xpath("//*[contains(text(), 'Yes')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Users | Masarka Student Club'))
    
    
def delete_user_item(driver, email):
    driver.get('https://is.mk.cvut.cz')
    open_admin_panel(driver, 'Users')
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '" + email + "')]")))
    driver.find_element_by_xpath("//*[contains(text(), '" + email + "')]").find_element_by_xpath("./../td[12]/button[4]").click()
    driver.find_element_by_xpath("//*[contains(text(), 'Yes')]").click()
    WebDriverWait(driver, 10).until(EC.title_contains('Users | Masarka Student Club'))
    

def clear_element_text(element):
    element.send_keys(Keys.CONTROL, 'a')
    element.send_keys(Keys.DELETE)
    
def wait_for_data_to_load(wait_time=1):
    time.sleep(wait_time)
    
def get_random_email(length=5):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(length)) + '@email.cz'


def test_login():
    with webdriver.Firefox() as driver:        
        login(driver, USERNAME, PASSWORD)        
        assert 'Profile | Masarka Student Club' in driver.title
        
def test_login_admin():
    with webdriver.Firefox() as driver:        
        login_admin(driver)        
        assert 'Profile | Masarka Student Club' in driver.title
        assert 'Admin panel' in driver.page_source
        
def test_logout():
    with webdriver.Firefox() as driver:        
        login_admin(driver)        
        assert 'Profile | Masarka Student Club' in driver.title
        logout(driver)
        assert 'Sign in | Masarka Student Club' in driver.title

def test_update_profile():
    with webdriver.Firefox() as driver:
        update_profile(driver)
        assert 'Profile | Masarka Student Club' in driver.title
        
def test_add_new_device():
    with webdriver.Firefox() as driver:
        add_new_device(driver)        
        assert 'AA:AA:AA:AA:AA:AA' in driver.page_source
        
def test_add_new_device():
    with webdriver.Firefox() as driver:
        add_new_reservation(driver)        
        assert 'Reservations | Masarka Student Club' in driver.title
        
def test_create_new_preregistration_item():
    with webdriver.Firefox() as driver:
        email = get_random_email()
        create_new_preregistration_item(driver, email)
        open_admin_panel(driver)
        wait_for_data_to_load()
        assert email in driver.page_source
        
def test_edit_preregistration_item():
    with webdriver.Firefox() as driver:        
        email = get_random_email()
        create_new_preregistration_item(driver, email)
        edit_preregistration_item(driver, email)
        wait_for_data_to_load()
        assert email in driver.page_source
        assert 'R001' in driver.page_source
        
def test_delete_preregistration_item():
    with webdriver.Firefox() as driver:
        email = get_random_email()
        create_new_preregistration_item(driver, email)                
        delete_preregistration_item(driver, email)        
        assert email not in driver.page_source
        
def test_complete_preregistration():
    with webdriver.Firefox() as driver:
        email = get_random_email()
        create_new_preregistration_item(driver, email)
        complete_preregistration(driver, email)
        wait_for_data_to_load()        
        assert email in driver.page_source
        
def test_edit_user_item():
    with webdriver.Firefox() as driver:        
        email = get_random_email()
        create_new_preregistration_item(driver, email)
        complete_preregistration(driver, email)        
        logout(driver)
        edit_user_item(driver, email)
        wait_for_data_to_load()
        assert email in driver.page_source
        assert 'R001' in driver.page_source
        
def test_delete_user_item():
    with webdriver.Firefox() as driver:        
        email = get_random_email()
        create_new_preregistration_item(driver, email)
        complete_preregistration(driver, email)        
        logout(driver)
        delete_user_item(driver, email)
        wait_for_data_to_load()
        assert email not in driver.page_source        



if __name__ == '__main__':
    test_login()
    test_login_admin()
    test_logout()
    test_update_profile()
    test_create_new_preregistration_item()
    test_delete_preregistration_item()
    test_edit_preregistration_item()
    test_complete_preregistration()
    test_edit_user_item()
    test_delete_user_item()
