import axios from '@/axios'
import { getHeaders } from '@/axios/headers'

export function getRegistrations () {
  return axios.get('/registrations', { headers: getHeaders() })
}
