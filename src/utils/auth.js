import store from '@/store'

export function isAuthorized (allowedRoles) {
  const user = store.getters['auth/getUser']

  if (!user) {
    return false
  } else {
    return allowedRoles.filter(value => user.roles.includes(value)).length > 0
  }
}
