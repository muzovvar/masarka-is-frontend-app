import store from '@/store'

export function getHeaders () {
  return {
    Authorization: 'Bearer ' + store.getters['auth/getToken']
  }
}
